const express = require('express');
const usersControllers = require('../controllers/usersControllers.js')
const productsControllers = require('../controllers/productsControllers.js')
const router = express.Router();
const auth = require("../auth.js")

// S42 - REGISTRATION ROUTE.
router.post("/register", usersControllers.registerUser);

// S42 - LOGIN ROUTE
router.get("/login", usersControllers.loginUser);

// S45 - RETRIEVE USER DETAILS (ADMIN ONLY)
router.get("/details", auth.verify, usersControllers.userDetails)

// S45 RETRIEVE USER DETAILS (CURRENTLY LOGGED IN)
router.get("/profile", auth.verify, usersControllers.myProfile)

// GET ALL USERS (ADMIN ONLY)
router.get("/all", auth.verify, usersControllers.getAllUsers)

// S45 - CREATE ORDER
router.post("/checkout", auth.verify, usersControllers.checkout)

// STRETCH GOAL - SET A USER AS ADMIN (ADMIN ONLY)
router.patch("/:userId/admin", auth.verify, usersControllers.setAsAdmin)

// STRETCH GOAL - REMOVE ADMIN PRIVILEGES (ADMIN ONLY)
router.patch("/:userId/dgAdmin", auth.verify, usersControllers.setAsUser)


module.exports = router;
const express = require('express');
const usersControllers = require('../controllers/usersControllers.js')
const productsControllers = require('../controllers/productsControllers.js')
const router = express.Router();
const auth = require("../auth.js")

// S43 - ADDING A PRODUCT ROUTE (ADMIN ONLY)
router.post("/addProduct", auth.verify, productsControllers.addProduct);

// S43 - RETRIEVING ALL PRODUCTS
router.get("/", productsControllers.getAllProducts);

// S43 - RETRIEVING ALL ACTIVE PRODUCTS
router.get("/activeProducts", productsControllers.getActiveProducts)

// S43 - RETRIEVING ALL INACTIVE PRODUCTS
router.get("/inactiveProducts", productsControllers.getInactiveProducts)

// [WITH PARAMS]

// S44 - UPDATE A PRODUCT INFORMATION (ADMIN ONLY)
router.patch("/:productId" , auth.verify, productsControllers.updateProduct)

// S44 - RETRIEVING A SINGLE PRODUCT
router.get("/:productId", productsControllers.getProduct)

// S44 - ARCHIVING/ACTIVATING A PRODUCT (ADMIN ONLY)
router.patch("/:productId/productStatus" , auth.verify, productsControllers.productStatus)

// S44 - ARCHIVING A PRODUCT (ADMIN ONLY)
router.patch("/:productId/archive", auth.verify, productsControllers.archiveProduct)

// S44 - ACTIVATING A PRODUCT (ADMIN ONLY)
router.patch("/:productId/activate", auth.verify, productsControllers.activateProduct)



module.exports = router;
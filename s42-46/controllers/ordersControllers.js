const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Orders = require("../models/Orders.js");
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");

// CREATE ORDER
module.exports.addToCart = (request, response) => {

// ------->

	

    	// console.log(productPrice)

// ^^^^^^^^^^^^^^^^^^^



    userData = auth.decode(request.headers.authorization)

    Orders.findOne({userId: userData.id})
    .then(result => {

// ------------------->


		let productPrice = Products.findById(request.body.productId)
    		.then(result => response.send((result.price).toString()))
    		.catch(error => response.send(error))

    	let amount = productPrice;



// ^^^^^^^^^^^^^^^^^^^^^^^^

    	if(result){
    		Orders.findOne({userId: userData.id})
    		.then(result=> {
    			result.products.push({
    				productId: request.body.productId,
		    		quantity: request.body.quantity,
		    		totalAmount: productPrice
		    	})
    			return response.send("Order added to your cart")
    			result.save()
           		.then(saved => true)
           		.catch(error => false)
    		}).catch(error => false)

    	} else {
    		let newOrder = new Orders({
    			userId: userData.id,
    			products: [{
		    			productId: request.body.productId,
		    			quantity: request.body.quantity,
		    			totalAmount: amount
		    		}]
    		})

    		newOrder.save()
    		.then(saved => response.send(`Congrats on your first order!. Your order is now added on your cart. Check your cart!`))
            .catch(error => response.send(error));

    	}
    }).catch(error => response.send(error));


}

// RETRIEVE ALL ORDERS (ADMIN ONLY)

module.exports.getAllOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Orders.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	} else {
		return response.send("For administrator's eyes only")
	}
}

// CART

// RETRIEVE AUTHENTICATED USER'S ORDER
module.exports.viewCart = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if(userData.id)
	Orders.find({ userId : userData.id })
	.then(result => response.send(result))
	.catch(error => response.send(error))
}


// CHANGE PRODUCT QUANTITIES
module.exports.updateProductQuantity = (request, response) =>{
	const userData = auth.decode(request.headers.authorization)
	
	let productId = {productId: request.body.productId}
	let updateQuantity = {quantity: request.body.quantity}
	
	if(userData.id){
	Orders.findOne({ userId : userData.id })
	.then(result => {
		Orders.findByIdAndUpdate(productId, updateQuantity)
		.then(result => response.send(`Product quantity changed to ${updateQuantity}`))
		.catch(error => response.send(error))
	})

	.catch(error => response.send(error))
	} else {
		return response.send("login to your account")
	}

}

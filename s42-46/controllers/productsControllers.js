const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Orders = require("../models/Orders.js");
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");


// SESSION 43 - CREATING/ADDING A PRODUCT (ADMIN ONLY)

module.exports.addProduct = (request, response) => {


	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
	
		let newProduct = new Products({
			productName: request.body.productName,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive
		})


		newProduct.save()
		.then(save => response.send("Product successfully added!"))
		.catch(error => response.send(error))
	} else {
		return response.send("Only administrators can add course!")
	}

}

// SESSION 43 -  RETRIEVING ALL PRODUCTS
module.exports.getAllProducts = (request, response) => {
	Products.find({})
	.then(result => response.send(result))
	.catch(error => response.send (error));
}

// SESSION 43 - RETRIEVING ALL ACTIVE PRODUCTS
module.exports.getActiveProducts = (request, response) => {
	Products.find({isActive:true})
	.then(result => response.send(result))
	.catch(error=> response.send(error));
}

// SESSION 43 - RETRIEVING INACTIVE PRODUCTS
module.exports.getInactiveProducts = (request, response) => {
	Products.find({isActive:false})
	.then(result => response.send(result))
	.catch(error=> response.send(error));
}

// SESSION 44 - RETRIEVE A SINGLE PRODUCT
module.exports.getProduct = (request, response) => {
	const productId = request.params.productId;
	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// SESSION 44 - UDPATE A PRODUCT INFORMATION (ADMIN ONLY)
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	
	let updatedProduct = {
		productName: request.body.productName,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(`Product information successfully updated!`))
		.catch(error => response.send(error))
	} else {
		return response.send("Sorry, only administrators can edit product information.")
	}
}

// SESSION 44 - ARCHIVE/ACTIVATE A PRODUCT (ADMIN ONLY)
module.exports.productStatus = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	let archiveProduct = {isActive: false}
	let activateProduct = {isActive: true}

	if(userData.isAdmin){
		Products.findById(productId)
		.then(result => {
			if(result.isActive){
				Products.findByIdAndUpdate(productId, archiveProduct)
				.then (result => response.send(`Active product successfully ARCHIVED.`))
				.catch (error => response.send(error))
			} else {
				Products.findByIdAndUpdate(productId, activateProduct)
				.then (result => response.send(`Archived product successfully ACTIVATED.`))
				.catch (error => response.send(error))
			}
		}).catch (error => response.send(error))

	} else {
		return response.send("For administrators only!")
	}

}

// SESSION 44 - ARCHIVE A PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	
	let archiveProduct = {isActive: false}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, archiveProduct)
		.then (result => response.send(`Product successfully archived`))
		.catch (error => response.send(error))
	} else {
		return response.send("You are not allowed to archive this product!")
	}
}


// ACTIVATE A PRODUCT (ADMIN ONLY)
module.exports.activateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	
	let activatedProduct = {isActive: true}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, activatedProduct)
		.then (result => response.send(`Product successfully activated`))
		.catch (error => response.send(error))
	} else {
		return response.send("You are not allowed to activate this product!")
	}
}

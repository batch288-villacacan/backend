const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Orders = require("../models/Orders.js");
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");

// SESSION 42 - USER REGISTRATION.

module.exports.registerUser = (request, response) => {
	Users.findOne ({email: request.body.email})
	.then(result => {

		if(result){
			return response.send(`${request.body.email} has been taken! Try logging in or sign up using different e-mail account!`)
		} else {
			let newUser = new Users ({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 9),
				isAdmin: request.body.isAdmin,
			})

			newUser.save()
			.then(saved => response.send (`${request.body.email} is now registerd!`))
			.catch(error => response.send(error));
		}
	})
	.catch(error => response.send(error));
}

// SESSION 42 - USER AUTHENTICATION

module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if (result){
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			
			if(isPasswordCorrect){
				return response.send({
					auth: auth.createAccessToken(result)
				})
			} else {
				return response.send("Please check your password.")
			}

		} else if(request.body.email == "") {
			return response.send("Please input your email.")
			
		} else {
			return response.send(`${request.body.email} is not yet registered.`)
		}
	})

	.catch (error => response.send(error));
}

// RETRIEVING USER DETAILS (ADMIN ONLY)

module.exports.userDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Users.findById(request.body.id)
		.then(result => {
			result.password = "****************";
			return response.send(result)
		})
		.catch (error => {
			return response.send(error)
		})
	} else {
		return response.send("You are not an admin, you are not authorized to view users' details")
	}
}

// SET A USER TO ADMIN
module.exports.setAsAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const userId = request.params.userId

	let nowAdmin = {
		isAdmin: true
	}

	if(userData.isAdmin){
		Users.findById(request.params.userId)
		.then(result =>{
			if(result.isAdmin === false){
				Users.findByIdAndUpdate(userId,nowAdmin)
				.then(result => response.send("congrats, user is now an admin"))
				.catch(error => response.send(error))
			} else {
				return response.send("User is already an admin")
			}
		})
		.catch(error => response.send(error))
	} else {
		return response.send("You are not allowed to change user roles.")
	}
}

// SET A ADMIN TO USER
module.exports.setAsUser = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const userId = request.params.userId

	let nowAdmin = {
		isAdmin: false
	}

	if(userData.isAdmin){
		Users.findById(request.params.userId)
		.then(result =>{
			if(result.isAdmin){
				Users.findByIdAndUpdate(userId,nowAdmin)
				.then(result => response.send("Admin privileges has been removed to the user"))
				.catch(error => response.send(error))
			} else {
				return response.send("User is not an admin")
			}
		})
		.catch(error => response.send(error))
	} else {
		return response.send("You are not allowed to change user roles.")
	}
}

// SHOW ALL USERS (ADMIN ONLY)
module.exports.getAllUsers = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Users.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	} else {
		return response.send("oopsie")
	}
	
}

// S45 - RETRIEVE USER DETAILS
module.exports.myProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	Users.findById(userData.id)
		.then(result => {
			return response.send(result)
		})
		.catch (error => {
			return response.send(error)
		})
	
}

// USER CHECKOUT
module.exports.checkout = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.body.id;
	const userId = Orders.userId;

	if(userData.isAdmin){
		return response.send("Only users can checkout products. Login to your user account.")
	} else {
		let userUpdated = Users.findOne({_id: userData.id})
		.then (Orders.find({}))
	}
}
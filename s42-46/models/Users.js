// SESSION 42 - DATA MODEL DESIGN.

const mongoose = require("mongoose");

// USER MODEL
const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "E-mail is required"]
	},

	password: {
		type: String,
		required: [true, "You need to have a password."]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	// orderedProduct: [{
	// 	products: [{
	// 		productId: {
	// 			type: String,
	// 			required: [true, "Product identification is required!"]
	// 		},

	// 		productName: {
	// 			type: String,
	// 			required: [true, "Product Name must be included."]
	// 		},

	// 		quantity: {
	// 			type: Number,
	// 			required: [true, "Input the number of products."]
	// 		}
	// 	}],

	// 	totalAmount: {
	// 		type: Number,
	// 		required: [true, "Total amount is missing. Kindly double check."]
	// 	},

	// 	purchasedOn : {
	// 		type: String,
	// 		default: new Date()
	// 	}
	// }]

}) //end USER MODEL

const Users = mongoose.model("User", userSchema);
module.exports = Users;
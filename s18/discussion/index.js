// console.log("Hello World!");
	
	function printInput(){
		let nickname = "Chris";

		console.log("Hi, " + nickname);
	}

	printInput();
	printInput();

	// For other cases, functions can also process data directly passed into it insted of relyingonly on Global Variables.

	// Consider this function:
		function printName(name){
		console.log("My name is " + name);
		}

		printName("Juana");

		printName();

	// variables can also be passed as an argument

		let sampleVariable = "Curry";

		let sampleArray = ["Davis", "Green", "Jokic", "Tatum"];

		printName(sampleVariable);
		printName(sampleArray);

	// One example of using the Parameter and Argument.
		//functionName(number);

	function checkDivisibilityBy8(num){
		let remainder = num % 8;

		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleBy8 = remainder === 0 ; 

		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);

	//Functions as Argument
		//Function parameters can also accept other functions as arguments.


	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed!")
	}


	function invokeFunction(func){
		func();
	}


	invokeFunction(argumentFunction);


	//Function as Argument with return keyword

	function returnFunction(){
		let name = "Chris";
	}


	function invokedFunction(func){
		console.log(func);
	}

	invokedFunction(returnFunction);

	// Using multiple parameters

	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}

	createFullName('Juan', 'Dela', 'Cruz');

	// In JavaScript, providing more/less arguments than the expected will not return an error


	// Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.
	createFullName('Gemar', null, 'Cruz');

	createFullName('Lito', "Masbate", "Galan", "Jr.");

	//In other programming languages, this will retrun an error stating that "the number of arguments do not match the number of parameters".

	//Using variable as Multiple arguments
	let firstName = "Anton";
	let middleName = "James";
	let lastName = "Villacacan";

	createFullName(firstName, middleName, lastName);


	function showSampleAlert(){
		alert("Hello, user!");
	}

	console.log("I will only..");
	// showSampleAlert();

	// Prompt
	// The value gathered from a prompt is returned as a string.

	// let samplePrompt = prompt("Enter your name.");
	// console.log(typeof samplePrompt);
	// console.log("Hello " + samplePrompt);

	// let age = prompt("Enter your age.");
	// console.log(age);
	// console.log(typeof age);

	// // let sampleNullPrompt = prompt("Don't enter anything.");
	// console.log(sampleNullPrompt);

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name.");
		let lastName = prompt("Enter your last name.");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");

	}

		printWelcomeMessage();
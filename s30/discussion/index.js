// [SECTION] MongoDB aggregation
/*
	use to generate manipulated data and perform oeprations to create filtered results that helps anlayizng data.

	compare to doing CRUD operations on our data from previous sessions,aggregation gives us results providing us with information to make necessary development decisions without having to create a frontend application.
*/

	db.fruits.aggregate([
		{$match : {onSale:true}},
		{$group : {_id: "$supplier_id", total: {$sum:"$stock"
		}}}
	])

	db.fruits.aggregate([
		{$match : {onSale : true}}
	])

	// The $group groups the documents in terms of the property declared in the _id property

	db.fruits.aggregate([
		{$group : {_id: "$supplier_id", total: {$sum:"$stock"
		}}}
	])


	// max operator
	db.fruits.aggregate([
		{$match : {onSale : true} },
		{$group :
			{_id: "$supplier_id",
			max : {$max : "$stock"},
			sum : {$sum: "$stock" }
			}
		}
	])

	db.fruits.aggregate([
		{$match : {color : "Red"} },
		{$group :
			{_id: "$supplier_id",
			max : {$max : "$stock"},
			sum : {$sum: "$stock" }
			}
		}
	])

	db.fruits.aggregate([
		{$match : {origin : "Philippines"} },
		{$group :
			{_id: "$supplier_id",
			max : {$max : "$stock"},
			sum : {$sum: "$stock" }
			}
		}
	])

// Field projection with aggregation
/*

*/

	db.fruits.aggregate([
		{$match : {origin : "Philippines"} },
		{$group :
			{_id: "$supplier_id",
			max : {$max : "$stock"},
			sum : {$sum: "$stock" }
			}
		},
		{$project : {_idkey: "value", 0} }
	])

// Sorting aggregated result


db.fruits.aggregate([
	{$match : {onSale : true} },
	{$group : {
		_id : "$supplier_id",
		total : {$sum : "$stock"}
	}},

// property to sort:total
	// 1 ascending
	// -1 descending
	{$sort : {total : -1} 	}
])

// Aggregating results based on an array field
// unwind operator
/*
	the $unwind operator deconstructs an array field from a collection/field with an array value to output a result for each element

	syntax:
	{$unwind : field}
*/

db.fruits.aggregate([
	{$unwind : "$origin"}


// Display fruit documents by their origin and the kinds of fruits that are supplied

// {$sum : 1} - it will count the number of documents in the group

	db.fruits.aggregate([
		{$unwind : "$origin"},
		{$group : {
			_id : "$origin",
			kinds : {$sum : 1}
		}}
	])

db.fruits.aggregate([
	{ $unwind : "$origin" },
	{ $group : {
		_id: "$origin",
		kinds: { $sum: 1 }
	}},
	{ $sort : { kinds : 1, _id: 1}}
])
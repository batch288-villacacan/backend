//  Advanced Queries

// Query an embedded document / object
db.users.find({
	contact: {
		phone : "87654321",
		email : "stephenhawking@gmail.com"
	}
})

// Dot notation
db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
});

// querying an array with exact element
db.users.find ({
	courses: ["CSS","Javascript","Python"]
});

db.users.find({courses: {
	$all: ["React"]
}})

db.users.find({courses: {
	$all: ['Laravel']
}})


// Query

// [Section] Comparison Query Operators
	// $gt/$gte operator
	/*
		allows us to find documents that have field number values greater than or equal to a specified value

		this operator will only work for number and integer data type

		syntax:
			db.collectionName.find ({field: {
				$gte: value
			}})

			db.collectionName.find ({field: {
				$gt: value
			}})
	*/

db.users.find({age : {$gt:72}});

db.users.find({age : {$gte:72}});

// $lt/$lte operator
	/*
		allows us to find documents that have field number values less than or equal to a specified value

		this operator will only work for number and integer data type

		syntax:
			db.collectionName.find ({field: {
				$lte: value
			}})

			db.collectionName.find ({field: {
				$lte: value
			}})
	*/

db.users.find({age: {$lt:76}});

// $ne operator
/*
	allows us to find documents that have field number values not equal to specified value

	syntax:
	db.collectinName.find({field: {$ne:value}});
*/

// $in operator (OR)
/*
	allows us to finde documents with specific mathc criteria one field using different values.
	syntax:
		db.collectionName.find({
			field:
				{
					$in:value
				}
		})

*/

db.users.find({lastName:{$in:["Hawking", "Doe"]}});

db.users.find({"contact.phone": {$in : ["87654321"]} });

db.users.find ({courses: {$in: ["React","Laravel"]}});

// [Section] Logical Query Operators
	// $or operator
	/*
		allows us to find documents that match a single criteria from multiple provided search criteria
		syntax:
			db.collectionName.find({
				$or: [{fieldA:valueA},{fieldB,valueB}]
			})
	*/

db.users.find ({
	$or: [{firstName:"Neil"},{age:21}]
})

// add multiple operators
	db.users.find({$or:[
		{"contact.phone":"87654321"},
		{age: {$gt: 50}}
		]
	});

// $and operator
	/*
		allows us to find documents that matching all the multiple criteria from multiple provided search criteria
		syntax:
			db.collectionName.find({
				$and:
				[
				{fieldA:valueA},
				{fieldB,valueB},
				...
				]
			})
	*/

db.users.find({
	$and:
	[
		{age:{$ne:82}},
		{age:{$ne:76}}
	]
});

// Mini-activity

	// age > 30
	// enrolled in css / html

db.users.find({
	$and:[
	{age:{$gt:30}},
	{courses: {$in: ["CSS","HTML"]}}
	]
})

// [Section] Field Projection
	/*
		-retrieving documents are common operations that we do and by default mongoDB queries return the whole document as a response.
	*/

	//Inclusion
	/*
		-allows us to include or add specific fields only when retrieving documents:

		Syntax:
		db.collectionName.find({criteria}, {field: 1});

	*/

db.users.find(
	{ firstName : "Jane"},
	{
		firstName: 1,
		lastName: 1,
		contact:1,
		_id: 0
	}

	)

db.users.find(
	{ firstName : "Jane"},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone":1,
		_id: 0
	}

	)

	// Exclusion
	/*
		allows us to exclude/remove specific fields olny when retrieving documents
		Syntax:
			db.users.find({criteria} , {field : 0})
	*/

db.users.find(
	{ firstName : "Jane"},
	{
		contact: 0,
		department: 0
	}
	)

db.users.find(
	{ $or: 
		[ { firstName : "Jane"},
			{age : {$gte : 30}}
			]
		},

	{
		"contact.email": 0,
		department: 0
	}
	)

db.users.find(
	{ $or: 
		[ { firstName : "Jane"},
			{age : {$gte : 30}}
			]
		},

	{
		"contact.email": 0,
		department: 0,
		courses: { $slice : 2}
	}
	)

// Evaluation Query Operator
	//$regex operator
		/*
			-allow us to find documents that match a specific string pattern using regular expressions.
			-syntax: 
			db.users.find({
					field: $regex: 'pattern', option : 'optionValue'
			})
		*/
// Case sensitive
db.users.find({firstName: { $regex: 'n'}});

//Case insensitive
db.users.find({firstName: { $regex: 'n', $options: 'i'}});

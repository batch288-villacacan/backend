// console.log("Hello World");

/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/
console.log("getUserInfo();")
 function getUserInfo(){
 	let userInfo = {
 		name: "Anton James Villacacan",
 		age: 28,
 		address: "Taguig City",
 		isMarried: true,
 		petName: "Snow",
 	}

  	console.log(userInfo);
}

getUserInfo();



/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/
	console.log("getArtistsArray();")
	function getArtistsArray() {
		let artists = ["Hokago Tea Time", "Kamikaze", "Zack Tabudlo", "Bandang Lapis", "Plethora"];

		return(artists);
	}

	let topArtists = getArtistsArray();
	console.log(topArtists);

/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	console.log("getSongsArray();")
	function getSongsArray() {
		let songs = ["Pasilyo","Habang Buhay","Pasilyo","Mahika","Unang Sayaw"];

		return(songs);
	}

	let topSongs = getSongsArray();
	console.log(topSongs);



/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	console.log("getMoviesArray();")
	function getMoviesArray() {
		let movies = ["Iron Man","Wall-E","Wreck-It-Ralph","Star Trek","Star Wars"];

		return(movies);
	}

	let topMovies = getMoviesArray();
	console.log(topMovies);


/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/
	console.log("getPrimeNumberArray();")
	function getPrimeNumberArray() {
		let prime = [2, 3, 5, 7, 11];

		return(prime);
	}

	let firstFivePrimes = getPrimeNumberArray();
	console.log(firstFivePrimes);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}
// console.log("Go!");

// [SECTION] JSON Objects
	// JSON stands for JavaScript Object Notation
	// JSON is also used in other programming languagse hence the name JavaScript Object Notation
	// Core JavaScript has a built in JSON object that contains method for parsing objects and converting strings into JavaScript objects.
	// .json file, JSON file
	// JavaScript objects are not to be confused with JSON
	// Serialization is the process of converting data into series of bytes for easier transmission/transfer of information/data.
	// A byte is a unit of data that is eight binary digits(1,0) that is used to reperesent a character (letters, numbers typographic symbols.)
	/*
	
		Syntax: {
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}

	*/

// [Section] JSON Arrays
/*

	"cities": [
		{"city":"Quezon City","province":"Metro Manila","country":"Philippines"}
	]

*/

// [Section] JSON Methods
	// The JSON object contains methods for parsing and converting data into stringified JSON.

	// Converting Data into Stringified JSON
		// Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript appllication
		// They are commonly used in HTTP request where information is required to be sent and receive in a stringified version
		// Requests are an important part of programming where an application communicate with a backed application to perform different tasks suchs as getting/creating data in a database.

let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}]

console.log(batchesArr);

// the stringify method is used to convert JavaScript Objects into a string.
console.log("Result from stringify method:");
// if you are going to convert objects to string, you'll use JSON.stringify(variableName);
console.log(JSON.stringify(batchesArr));

// Using stringify method with variables
	/*

		Syntax:
			JSON.stringify ({
				propertyA:variableA,
				propertyB:variableB,
			})
	*/

// User details
// let firstName = prompt('What is your first name?');
// let lastName = prompt('What is your last name?');
// let age = prompt('How old are you?');
// let address = {
// 	city: prompt('What city are you from?'),
// 	country: prompt('From which country are you?')
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// });

// console.log(otherData);

// Converting stringified into JavsScript Objects
// Objects are common data types used in applications because of the complex data structures that can be created out of them

let batchesJSON = '[{"batchNumber":"288"},{"batchNumber":"888"}]'

console.log('Result form parse method:');
console.log(JSON.parse(batchesJSON));
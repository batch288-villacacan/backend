// To check whether the script file is properly linked to your html file.
/*console.log("Hello Batch 288!");*/

// [Section] Arithmetic Operators
	let x = 1397;
	let y = 7831;

	// Addition Operator(+)
	let sum = x + y;

	console.log("Result of addition operator: " + sum);

	// Subtraction Operator (-)
	let difference = y - x;
	console.log("Result of subtraction operator: " + difference);

	// Multiplication Operator (*)
	let product = x * y;
	console.log("Result of multiplication operator: " +product);
	//Division Operator (/)
	let quotient = y / x;
	console.log("Result of division operator: " + quotient.toFixed(2));

	// Modulo Operator (%)
	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// [Section] Assignment Operators 
	// Assignment Operator (=)
	// The assignment operator assigns/reassign the value to the variable.

	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable

	assignmentNumber += 2;

	console.log(assignmentNumber);

	assignmentNumber += 3;
	console.log("Result of addition assingment Operator: " + assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator(-=, *=, /=)

	// Subtraction Assigment Operator
	assignmentNumber -= 2;
	console.log("Result of subtraction assingment operator: " + assignmentNumber);

	//Multiplication Assignment Operator
	assignmentNumber *= 3;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	// Division Assignment Operator
	assignmentNumber /= 11;
	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses
	// MDAS - Mutiplication or Division First then Addition or Subtraction, from left to right.
	let mdas = 1 + 2 - 3 * 4 / 5;
	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		1 + 2 - 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/


	console.log(mdas.toFixed(1));

	let pemdas = 1 + (2-3) * (4/5);
	/*
		1. 4/5 = 0.8
		2. 2-3 = -1
		1 + (-1) * (0.8)
		3. -1 * 0.8 = -0.8
		4. 1 - 0.8
		0.2
	*/

	console.log(pemdas.toFixed(1));

//[Section] Increment and Decrement
	//Operators that add or subtract values by 1 and reassings the value of the variable where the increment and decrement applied to.

	let z = 1;

	let increment = ++z;

	console.log("Result of pre-increment: " + increment);

	console.log("Result of pre-incerement: " + z);

	increment = z++;
	console.log("The result of post-increment: " + increment);

	console.log("The result of post-increment: " + z);

	x = 1;


	let decrement = --x;

	console.log( "Result of pre-decrement: " + decrement);
	console.log( "Result of pre-decrement: " + x);

	decrement = x--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + x);

// [Section] Type Coercion
	/*
		-Type Coercion is the automatic or implicit conversion of values from one data type to another.
		-This happens when operations are performed on different data types that would normally possible and yield irregular results.
	*/

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	// IF you are going to add string and number, it will concatenate its value
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

	let juan = 'juan';
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true
	console.log('juan' == 'juan'); //true

	

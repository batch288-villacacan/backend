const express = require("express");

// Mongoose is a package that allows creation of Schemas to Model our data structures
// Also has access to a number of methods for manipulating database
const mongoose = require('mongoose')
const app = express();
const port = 3001;

// [Section] MongoDB Connection
// Connect to the database by passing your connection string
// Due to update in MongoDB drivers that allow connection, the default connection string being flagged as an error
// by default a warning will be displayed in the terminal when the application is running
// {useNewUrlParser: true};

	mongoose.connect("mongodb+srv://admin:admin@batch288villacacan.yzbkoln.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

	// Notification whether we connected properly with the database.

	let db = mongoose.connection;

	// for catching the error just in case we had an error during the connection
	// console.error.bind allows us to print error in the browser and in the terminal
	db.on("error", console.error.bind(console, "Error! Can't connect to the database"));

	// if the connection is successful:
	db.once("open", ()=> console.log("We're connected to the cloud database!"));

// Syntax:
// mongoose.connect("MongoDB string", {use NewUrlParser:true});

	// Middlewares

	app.use(express.json());

	app.use(express.urlencoded({extended:true}));

	// [Section] Mongoose Schemas
	// Schemas determine the structure of the document to be written in the database
	// Schemas act as a blueprint to our data
	// Use the Schema() constructor of the mongoose module to create a new Schema object.

	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type
		name: String,
		// let us add another field which is status
		status: {
			type: String,
			default: "pending"
		}
	})

	// [Section] Models
	// Uses schema and are used to create/instantiate objects that corresponds to the schema.
	// Models use Schema and they act as the middleman from the server(JS code) to ur database.
	// allows us to use CRUD operation to our database

	// To create a model we are going to use the model();

	const Task = mongoose.model('Task', taskSchema);
	   // ^capital to indicate model

	// Section: Routes

	// Create a POST route to create a new task
	// Create a new task
	// Business Logic:
		// 1. Add a functionality to check whether there are duplicate tasks
			// if the task is existing in the database, we return an error
			// if the task does exist in the database, we add it in the database
		// 2. The task data will be coming from the request's body

	app.post("/tasks", (request, response) => {
		Task.findOne({name: request.body.name})
		.then(result => {
			// we can use if statement to check or verify whether we have an object found.
			if(result !== null){
				return response.send("Duplicate task found!")
			} else {
				// Create a new task and save it to the database.
				let newTask = new Task({
					name: request.body.name
				})

				newTask.save();
				return response.send('New task created!');
			}


		})
	})

	// Get all the tasks in our collection
	// 1. Retrieve all the documents
	// 2. If an error is encountered, print the error
	// 3. If no error/s is/are found, send a success status to the cliend and show the documents retrieved

	app.get("/tasks", (request, response) => {
		// the find method is a mongoose method that is similar to MongoDB find
		Task.find({}).then(result => {
			return response.send(result);
		}).catch(error => response.send(error));

	})


if (require.main === module{
	app.listen(port, () => {console.log(`Server running at ${port}`)
	})
})

module.exports = app;
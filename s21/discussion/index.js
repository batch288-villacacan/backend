console.log("Test");

// An Array in programmin is simply a list of data that shares the same data type.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Now, with an array, we can simply write the code above like this:

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// [Section] Arrays
	// Arrays are used to store multiplerelated values in a single variable.
	// They are declared using the square brackets ([]) also known as "Array Literals"
/*

	Syntax"
		let/const arrayName = [elementA, elementB, ...];

*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Toshiba'];

// Possible use of an array but not recommended

let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);	

// Alternative way to write arrays"
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
	];

console.log(myTasks);

// Creating an array with values from variable.

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(cities);

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also set the total number of elements in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length of an array.

console.log(myTasks);

// myTasks.length = myTasks.length - 1;

myTasks.length -= 2;

console.log(myTasks);
console.log(myTasks.length);

// To delete a specific item in an array we can employ array methods or an algorithm.
// Another example using decramentation:
console.log(cities);
cities.length--;
console.log(cities);

// We can't do the same on strings
let fullName = "Jamie Noble";
console.log(fullName);
fullName.length -= 1;
console.log(fullName);

// Since you can shorten the array by setting the length property, you can also lengthen it by adding a number into the lemgth property. Since we lengthen the array forcible, there will be another item in the array, however, it will be empty or undefined.

let theBeatles = ["John", "Ringo", "George"]
console.log(theBeatles);

theBeatles.length += 1;
console.log(theBeatles);

// [Section] Reading fromArrays

/*
	-Accessing array elements is one of the more common tasks that we can do with an array
	- We can do this by using the array indexes.
	- Each element in an array is associated with its own index/number.

	Syntax:
		arrayName[index]

*/

console.log(grades[0]);
 let lakersLegends = ["Kobe", "Shaq","Lebron", "Magic","Kareem"];
 console.log(lakersLegends);

// You can save/store array items in another variable

 let currentLaker = lakersLegends[2];

 // You can reassign array values using item's indices

 console.log('Array before reassignment: ');
 console.log(lakersLegends);

 // Reassign value of specific element:
lakersLegends[1] = "Pau Gasol";
console.log("Array after reassignment: ");
console.log(lakersLegends);

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

console.log(bullsLegends [bullsLegends.length-1]);

// Adding elements into the Array

let newArr = [];
console.log(newArr);
newArr[newArr.length] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);

// [Section] Looping over an array
	//  You use a for loop to iterate over all items in an array

console.log("______________");

	// for loop
for (let index = 0; index < bullsLegends.length; index++){
	console.log(bullsLegends[index]);
}


// Mini-Activity

// If divisible by 5 console the number + "is divisible by 5"
// If not, console the number + "is not divisible by 5"

let numDivBy5 = [];
let numNotDivBy5 = [];

let numArr = [5,12,30,46,40];

for (let index = 0; index < numArr.length; index ++){

	if (numArr[index]%5 === 0) {
		console.log(numArr[index] + " is divisible by 5");
		numDivBy5[numDivBy5.length] = numArr[index]
	}

	else {
		console.log(numArr[index] + " is not divisible by 5");
		numNotDivBy5[numNotDivBy5.length] = numArr[index]
	}
}

console.log(numDivBy5);
console.log(numNotDivBy5);

let chessboard = [
	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8'],
	]

console.log(chessboard);

console.log(chessboard[0][2]);

// two dimensional array
console.table(chessboard);
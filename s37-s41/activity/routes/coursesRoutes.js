const express = require("express");
const coursesControllers = require("../controllers/coursesControllers.js")

const router = express.Router();
const auth = require("../auth.js")

// This route is responsible for adding course in our db.

router.post("/addCourse", auth.verify, coursesControllers.addCourse);

// Route for retrieving all courses
router.get ('/', auth.verify, coursesControllers.getAllCourses);

// Route for retrieving all courses
router.get('/activeCourses', coursesControllers.getActiveCourses);

router.get('/inactiveCourses', auth.verify, coursesControllers.getInactiveCourses)

// WITH PARAMS

router.get('/:courseId', coursesControllers.getCourse);

// route for updating course
router.patch('/:courseId', auth.verify, coursesControllers.updateCourse)

// archive
router.patch ("/:courseId/archive", auth.verify, coursesControllers.archiveCourse)

// MAGKAKASAMA LAHAT NG PARAMS AT MAGKAKASAMA LAHAT NG WALANG PARAMS

module.exports = router;
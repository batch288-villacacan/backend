// para magamit router 
const express = require('express');
const usersControllers = require('../controllers/usersControllers.js')

const router = express.Router();
const auth = require("../auth.js")

//routes
router.post("/register", usersControllers.registerUser);

// login
router.post("/login", usersControllers.loginUser);

// user details
router.get("/details", auth.verify, usersControllers.userDetails);

// tokenVerification
// router.get("/verify", auth.verify);

// user details
router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

// // get Profile
// router.get("/details", auth.verify, usersControllers.getProfile);

// 
router.post("/enroll", auth.verify, usersControllers.enrollCourse);


module.exports = router;
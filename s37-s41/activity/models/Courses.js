// First, require the mongoose dependency
const mongoose = require("mongoose");

// Schema
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required!"]
	},
	
	description: {
			type: String,
			required: [true, "Course description is required!"]
		
	},

	price: {
			type: Number,
			required: [true, "Course price is required!"]
	},

	isActive: {
		type: Boolean,
		required: [true, "Course status is required!"],
		default: true
	},

	createdOn: {
		type: Date,
		// The new Date() expression instantiates a new date that stores the current date and time whenever a course is created.
		default: new Date()
	},

	slots: {
		type: Number,
		required: [true, "Course slots is required!"]
	},

	enrollees: [
			{
				userId: {
					type: String,
					required: [true, "Uder ID of the enrollee is required!"]
				},

				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
})

// Model
const Courses = mongoose.model("Course", courseSchema);

module.exports = Courses;
// dito tayo gagawa ng function na magcrecreate ng token
// also yung pagdecode din ng token, dito yun gagawin

const jwt = require("jsonwebtoken");

// User defined string data that will be used to create our JSON web token.
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined keyword.

const secret = "CourseBookingAPI";

// [SECTION] JSON web token
// Json web token or JWT is a way of securely passing information from the server to the frontend or to the parts of server.
// Information is kept secure through the use of the secret code.

// Analogy:
/*
	Pack the gift and provide a lock with the secret code as the key.
*/

// The argument that will be passed in the parameter to be the document or object that contains the info of the user.

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}

	// Generate a JSON web token using JWT's sign method
	// Generate the token using the form data and the secret code with no additional options provided.

	return jwt.sign(data, secret, {});
}

// Token Verification
/*
	Analogy:
		Receive gift and open the lock to verify if the sender is legitimate and the gift was not tampered
*/

module.exports.verify = (request,response, next) => {
	let token = request.headers.authorization;
	// console.log(token);
	// console.log(request);

	if(token !== undefined){
		token = token.slice(7, token.length);

		// Validate the token using the verify method in decrypting the token using the secret code
		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return response.send(false)
			} else {
				next();
			}
		})
		} else {
			return response.send (false);
		}
	}

// decode the  decrypted token.
module.exports.decode = (token) => {
	token = token.slice(7, token.length);

	// the decode method is use to obtain the information from the JWT
	// the {complete:true} option allows us to return addtional information from JWT

	return jwt.decode(token, {complete: true})
	.payload;
}
const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js")
const bcrypt = require("bcrypt");
const auth = require ("../auth.js");

// Controllers

// Create a controller for the signup
// registerUser

/* Business Logic/ Flow 
    1. First, we have to validate whether the user is existing or not.
    We can do that by validating whether the email exist on our database or not.
    2. If the user email is existing, we will prompt an error telling the user that the email is taken.
    3. Otherwise, we will sign up or add the user in our database.
*/
module.exports.registerUser = (request, response) => {

    Users.findOne({email: request.body.email})
    .then(result => {

        // we need to add if statement to verify whether the email exists
        if(result){
            return response.send(false)
        }
        else{

            // Create a new object instantiated using the Users Model.
            let newUser = new Users({
                firstName: request.body.firstName,
                lastName: request.body.lastName,
                email: request.body.email,

            // hashSync method: it has/encrypt our password
            // the second argument salt rounds

                password: bcrypt.hashSync(request.body.password, 10),
                isAdmin: request.body.isAdmin,
                mobileNumber: request.body.mobileNumber
            })

            // save the user
            // error handling
            newUser.save()
            .then(saved => response.send(true))
            .catch(error => response.send(false));
        }
    })
    .catch(error => response.send(error));

}

// new controller for the authentication of the user

module.exports.loginUser = (request, response) => {
    Users.findOne({email: request.body.email})
    .then(result => {
        if (!result){
            return response.send(false)
        } else {
            // the compareSync method is used to compare a non encrypted password form from the login form to the encrypted password retrieve from the find method. Returns true or false depending on the result of the comparison.

            // compareSync(unencrypted, encrypted)
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

            if(isPasswordCorrect){
                return response.send({
                auth: auth.createAccessToken(result)
            })
            } else {
                return response.send(false)
            }
        }
    })
    .catch(error => response.send(false));
}

module.exports.userDetails = (request, response) => {

    const userData = auth.decode(request.headers.authorization)

    // console.log(userData)

    if(userData.isAdmin){
            Users.findById(request.body.id)
            .then(result => {
                result.password ="";
                return response.send(result)
            })

            .catch(error => {
                return response.send(error);
            })
    } else {
            return response.send("You are not an admin, you don't have access to this route.")
    }
}

// Controller for the enroll course.

module.exports.enrollCourse = (request, response) => {

    const courseId = request.body.id;
    const userData = auth.decode(request.headers.authorization)

    if(userData.isAdmin){
        return response.send(false);
    }else{
        // Push papunta kay user document
       let isUserUpdated = Users.findOne({_id : userData.id})
        .then(result => {
            console.log(result.enrollments)
            result.enrollments.push({
                courseId : courseId
            })
            
            result.save()
            .then(saved => true)
            .catch(error => false)
        })
        .catch(error => false)


        // Pushing to course document

        let isCourseUpdated = Courses.findOne({_id : courseId})
        .then(result => {
            result.enrollees.push({userId: userData.id});
            
            result.save()
            .then (saved => true)
            .catch (error => false)
        })
        .catch(error => false)

        //  if conditon to check whether we updated the users document and courses document
        if ( isUserUpdated && isCourseUpdated ) {
            return response.send(true)
        } else {
            return response.send(false)
        }
    }
}

module.exports.retrieveUserDetails = (request, response) => {
    const userData = auth.decode(request.headers.authorization);

    Users.findOne({_id : userData.id})
    // .then(result => result.toJSON())
    .then(data => response.send(data))
}
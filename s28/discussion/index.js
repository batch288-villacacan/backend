// show databases - list of db inside our cluster
// use 'dbName' - to use a specific database
// show collections -  to see the list of collections inside the db.

// [SECTION] Insert One Document (Create)

/*db.collectionName.insertOne({
	object
})
*/


db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS","JavaScript","python"],
	department: "none"
})


// Insert Many
/*
	syntax:
		db.collectionName.insertMany([{objectA},{objectB}]);
*/

db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"	
		},

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	])

db.userss.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"	
		},

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	])

// [SECTION] Finding Documents (Read Operation
	// db.collectionName.find();
	// db.collectionName.find({field:value});
// Using the find() method, it will show you the list of all the documents inside our collection.

db.users.find();

// The "pretty" methods allows us to be able to view the documents in a pretty format.
db.users.find().pretty;

// it will return the documents that will pass the criteria given in the method
db.users.find({firstName: "Stephen"});

// multiple criteria
db.users.find({lastName : "Armstrong", age : 82});

db.users.find({firstName:"Jane"});

// [SECTION] Updating documents(Update)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

// updateOne method
/*
	Syntax:
	db.collectionName.updateOne({criteria},{$set:{field:value}})
*/

db.users.updateOne(
{firstName: "test"},
{
	$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone:"12345678",
			email:"bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "none"
	}
}
)

db.users.updateOne(
{firstName: "Bill"},
{
	$set: {
		firstName: "James",
	}
}
)

db.users.updateOne(
{firstName: "Jane"},
{
	$set: {
		firstName: "Edited",
	}
}
)

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({
			{criteria},
			{
				$set: {
					{field:value}
				}
			}
		})
*/

db.users.updateMany (
	{department:"none"},
	{
		$set: {
			department:"HR"
		}
	}

	)

// Replace One
/*
	Syntax: db.collectionName.replaceOne(
	{critereia},
		$set: {
			object
		}
	)
*/

db.users.insertOne({firstName: "test"});

db.users.replaceOne(
	{firstName: "test"},
	
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {},
			courses: [],
			department: "Operations"
		}
	
	)

// [SECTION] Deleting document/s

	// db.collectionName.deleteOne ({criteria})

	db.users.deleteOne({firstName:"Bill"});

	// Deleting multiple document

		// db.collectionName.deleteMany({criteria})

	db.users.deleteMany({firstName: "Jane"});

	// All the documents in our collection will be deleted
	db.users.deleteMany({});
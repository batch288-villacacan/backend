// console.log("Hello Batch 288");

// [Section] JavaScritp Synchronous vs Asynchronous
	// Javascript is by default is synchronous meaning that only one statement is executed a time.

	// This can be proven when a statement has an error, javascript will not proceed with the next statement

/*console.log("Hello World!");

conole.log("Hello after the world");

console.log("Hello");

*/

	// When certain statements take a lot of time to process, this slows down our code.

	/*for (let index = 0; index <= 150000 ; index++){
		console.log(index);
	}

	console.log("Hello Again!");*/

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// [Section] Getting all posts
	// The Fetch API allows you to asynchronously request for a resource data.
	// It means that the fetch method we are going to use here will run asynchronously.
	// Syntax:
		// fetch ('URL');
	
	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
	/*
		fetch('URL')
		.then((response => response));

	*/




// start


	fetch('https://jsonplaceholder.typicode.com/posts')
	// the fetch moethod will return a prmise that resolves the Response object.

	// the "then" method captures the response object and returns a
	// use the "json" method from the Response object to convert the data retrieved into JSON format to be used in our application
	// .then(response => {
		// console.log(response.json())})

	/*.then(response => {
		return response.json()
	})*/

	// gawin nating one line para mag take effect ang implicit return ni arrow function
	.then(response => {	return response.json()})
	.then(json => console.log(json))

	// The "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code

	// Creates an asynchronous function

	async function fetchData(){
		let result = await fetch('https://jsonplaceholder.typicode.com/posts')
		console.log(result);

		let json = await result.json();
		console.log(json);
	};

	fetchData();

// end

	// [Section] Getting specific post
	// Retrieves specific post following the rest API(/posts/:id)

	fetch('https://jsonplaceholder.typicode.com/posts/5')
	.then(response => response.json())
	.then(json => console.log(json))

// [Section] Creating Post
// Syntax:
	/*
		options is an object that contains the method, the head and the body of the request
		
		by default, if you don't add the method in the fetch request, it will be a GET method.

		fetch('URL', options)
		.then (response => {})
		.then (response => {})
	*/

		fetch('https://jsonplaceholder.typicode.com/posts', {
			// sets the method of the request object to post following the rest API
			method: 'POST',
			// sets the header data of the request object to be sent to the backend
			// specified that the content will be in JSON structure
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: 'New post',
				body: 'Hello World',
				userId: 1
			})
		})
		.then(response => response.json() )
		.then(json => console.log(json));

		// [Section] Update a specific post
/*
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},

			body: JSON.stringify({
				id: 1,
				title: 'Updated Post',
				body: 'Hello again!',
				userId: 1
			})
		})
		.then(response => response.json() )
		.then(json => console.log(json))*/

		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},

			body: JSON.stringify({
			
				title: 'Updated Post',
				
				
			})
		})
		.then(response => response.json() )
		.then(json => console.log(json))

		// PATCH
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},

			body: JSON.stringify({
				
				title: 'Updated Post',
				
				
			})
		})
		.then(response => response.json() )
		.then(json => console.log(json))

		// The PUT method is a method of modifying resource where the client sends data that updates the entire object/document

		// Patch method applies a partial update to the object or document.


	// [Section] Deleting a post.

		fetch('https://jsonplaceholder.typicode.com/posts/1', {method: "DELETE"})

		.then(response => response.json() )
		.then(json => console.log(json))


		/*.then(json => {
			// map
		})*/
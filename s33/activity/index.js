// console.log("Test");

//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

		fetch('https://jsonplaceholder.typicode.com/todos/1')
       		.then((response) => response.json())
       		.then(json => {
       			return json
       		})

   );
 }

async function getAllToDo(){

	return await (

		fetch('https://jsonplaceholder.typicode.com/todos/',{
			method:"GET",
			headers:{
				'Content-Type' : 'application.json'
				}
			})
		.then((response) => response.json())
		.then(json => {
			return json.map(item => item.title)
		})
	);
}


async function getSpecificToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos/4',{
			method:"GET",
			headers:{
				'Content-Type' : 'application.json'
				}
			})
		.then((response) => response.json())
		.then(json => json)
		
   );

}


async function createToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos/',{
			method:"POST",
			headers:{
				'Content-Type' : 'application.json'
			},

			body: JSON.stringify({
				userId: 5,
				title: "New To-do!",
				id: 5,
				completed: true
			})
		})
		.then((response) => response.json())
		.then(json => json)


   );

}	


async function updateToDo(){
   
   return await (

		fetch('https://jsonplaceholder.typicode.com/todos/1',{
			method:"PUT",
			headers:{
				'Content-Type' : 'application.json'
			},

			body: JSON.stringify({
				title: "Naks, done!",
				description: "All right",
				status: "partially completed",
				dateCompleted: "05/28/23",
				userId: 452
			})
		})
		.then((response) => response.json())
		.then(json => json)
	);
}


async function deleteToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos/2', {method: "DELETE"})

		.then(response => response.json() )
		.then(json => {return json})


   );

}

//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}
// 1. What directive is used by Node.js in loading the modules it needs?

	// Answer: Require directive is used by Node.js in loading the modules it needs.

// 2. What Node.js module contains a method for server creation?

	// Answer: The Node.js http module contains a method for server creation.

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// Answer: The http.createServer() method is the method of the http object responsible for creating a server using Node.js.

// 4. What method of the response object allows us to set status codes and content types?
	
	// Answer: The .writeHead() method of the response object allows us to set status codes and content types.

// 5. Where will console.log() output its contents when run in Node.js?

	// Answer: The console.log() will output its contents when run in Node.js in the Git Bash console.

// 6. What property of the request object contains the address's endpoint?

	// Answer: the request.url property of the request object contains the address's endpoint
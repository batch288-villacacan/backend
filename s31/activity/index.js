const http = require('http');
const port = 3000;

const server = http.createServer((request, response) => {
	if (request.url == '/login'){
		response.writeHead ( 200, {'Content-Type': 'text/plain'} );
		response.end ('Welcome to the login page.');
	} else if (request.url == '/register'){
		response.writeHead( 404, {'Content-Type': 'text/plain'});
		response.end('The page you are looking for cannot be found.');
	} else {
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end('Page is not available. Try refining your search');
	}
})

server.listen(port);
console.log(`Server is now accessible at localhost:${port}.`);
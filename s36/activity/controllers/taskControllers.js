const Task = require('../models/task.js')

/*Controllers*/

// This controller will get/retrieve all the document from the tasks collection

module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// Create a controller that will add data in our database
module.exports.addTasks = (request, response)=>{

	Task.findOne({"name" : request.body.name})
	.then(result =>{
		if(result != null){
			return response.send("Duplicate Task");
		} else {
			let newTask = new Task({
				"name": request.body.name
			})

			newTask.save();

			return response.send("New task created!")
		}
	}).catch(error => response.send(error))
}

module.exports.deleteTask = (request,response) => {
	console.log(request.params.id);
	// In mongoose, we have the findByIdAndRemove method that will look for a document with the same id provided from the URL and remove/delete the document from the MongoDB

	let taskToBeDeleted = request.params.id;

	Task.findByIdAndRemove(taskToBeDeleted)
	.then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted!`)
	}).catch(error => response.send(error))
}

// Activity

// Retrieving a specific task

module.exports.getSpecificTask = (request, response) => {
	let specficTask = request.params.id;

	Task.findById(specficTask)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// Update a status of a task from pending to complete
module.exports.completeTask = (request, response) => {
	
	let taskToBeCompleted = request.params.id;
	
	Task.findByIdAndUpdate(taskToBeCompleted,{
		"status": "complete"
	})
	
	.then (result => {
		return response.send(`The task with an _id ${taskToBeCompleted} has been completed!`)
	}).catch(error => response.send(error))
}
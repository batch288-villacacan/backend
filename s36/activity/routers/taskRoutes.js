


// Contain all the endpoints of our application
const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js");
module.exports = router;
router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks);

// Parameterized

// We are to create a route using a Delete method at the URL "/tasks/:id"
// The colon here is an identifier that helps to create a dynamic route which allows us to supply information
router.delete("/:id", taskControllers.deleteTask);

// Activity

router.get("/:id", taskControllers.getSpecificTask);

router.put("/:id/complete", taskControllers.completeTask);

module.exports = router;